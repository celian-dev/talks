#!/bin/sh

set -e

cd "$(dirname "$(realpath "$0")")"/..

rm -rf public
cp -r src public

lib/scripts/public-install.sh
